<?php
/**
 * Kivi
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the kivi.co.il license that is
 * available through the world-wide-web at this URL:
 * http://kivi.co.il/license-agreement/
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Kivi
 * @package     Kivi_FacebookShop
 * @copyright   Copyright (c) 2014 Kivi (http://kivi.co.il/)
 * @license     http://kivi.co.il/license-agreement/
 */

/**
 * FacebookShop Index Controller
 * 
 * @category    Kivi
 * @package     Kivi_FacebookShop
 * @author      Kivi Developer
 */
class Kivi_FacebookShop_IndexController extends Mage_Core_Controller_Front_Action
{
    /**
     * index action
     */
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

	public function apiAction()
	{
		$post = Mage::app()->getRequest()->getPost();
		$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : null;
		$result = array('status'=>101);
		if($action){
			$api = Mage::getModel('facebookshop/api');
			switch($action){
				case 'configs':
					$result = $api->getConfigs();
					break;
				case 'config':
					$result = isset($_REQUEST['config_name']) ? $api->getConfig($_REQUEST['config_name']) : '';
					break;
				case 'getProducts':
					$result = $api->getProducts();
					break;

				case 'getCategories':
					$result = $api->getCategories();
					break;
				case 'getMenuItems':
					$result = $api->getMenuItems();
					break;

				case 'getCategory':
					$result = $api->getCategory(Mage::app()->getRequest()->getParam('id'));
					break;




			}

			echo $api->encode($result);
		}

	}
}