<?php
/**
 * Kivi
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the kivi.co.il license that is
 * available through the world-wide-web at this URL:
 * http://kivi.co.il/license-agreement/
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Kivi
 * @package     Kivi_FacebookShop
 * @copyright   Copyright (c) 2014 Kivi (http://kivi.co.il/)
 * @license     http://kivi.co.il/license-agreement/
 */

/**
 * Facebookshop Block
 * 
 * @category    Kivi
 * @package     Kivi_FacebookShop
 * @author      Kivi Developer
 */
class Kivi_FacebookShop_Block_Facebookshop extends Mage_Core_Block_Template
{
    /**
     * prepare block's layout
     *
     * @return Kivi_FacebookShop_Block_Facebookshop
     */
    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
}