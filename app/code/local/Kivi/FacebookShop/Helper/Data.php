<?php
/**
 * Kivi
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Kivi.com license that is
 * available through the world-wide-web at this URL:
 * http://kivi.co.il/license-agreement/
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Kivi
 * @package     Kivi_FacebookShop
 * @copyright   Copyright (c) 2014 Kivi (http://kivi.co.il/)
 * @license     http://kivi.co.il/license-agreement/
 */

/**
 * FacebookShop Helper
 * 
 * @category    Kivi
 * @package     Kivi_FacebookShop
 * @author      Kivi Developer
 */
class Kivi_FacebookShop_Helper_Data extends Mage_Core_Helper_Abstract
{
 
 	protected function _initConfig()
    {
        return Mage::getSingleton('facebookshop/config');
    }

    public function isEnabled(){
        return $this->_initConfig()->isEnabled();
    }

}