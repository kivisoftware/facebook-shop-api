<?php
/**
 * Kivi
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Kivi.com license that is
 * available through the world-wide-web at this URL:
 * http://kivi.co.il/license-agreement/
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Kivi
 * @package     Kivi_FacebookShop
 * @copyright   Copyright (c) 2014 Kivi (http://kivi.co.il/)
 * @license     http://kivi.co.il/license-agreement/
 */

/**
 * FacebookShop Status Model
 * 
 * @category    Kivi
 * @package     Kivi_FacebookShop
 * @author      Kivi Developer
 */
class Kivi_FacebookShop_Model_Status extends Varien_Object
{
    const STATUS_ENABLED    = 1;
    const STATUS_DISABLED    = 2;
    
    /**
     * get model option as array
     *
     * @return array
     */
    static public function getOptionArray()
    {
        return array(
            self::STATUS_ENABLED    => Mage::helper('facebookshop')->__('Enabled'),
            self::STATUS_DISABLED   => Mage::helper('facebookshop')->__('Disabled')
        );
    }
    
    /**
     * get model option hash as array
     *
     * @return array
     */
    static public function getOptionHash()
    {
        $options = array();
        foreach (self::getOptionArray() as $value => $label) {
            $options[] = array(
                'value'    => $value,
                'label'    => $label
            );
        }
        return $options;
    }
}