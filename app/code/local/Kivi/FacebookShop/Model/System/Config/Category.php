<?php
class Kivi_FacebookShop_Model_System_Config_Category
{

    public function toOptionArray($addEmpty = true)
    {

        $collection = Mage::getSingleton('catalog/category')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('level', 2)
        ;

        $options = array();

        if ($addEmpty) {
            $options[] = array(
                'label' => Mage::helper('adminhtml')->__('-- Please Select a Category --'),
                'value' => ''
            );
        }
        foreach ($collection as $category) {
            $options[] = array(
                'label' => $category->getName(),
                'value' => $category->getId()
            );
        }

        return $options;
    }

}