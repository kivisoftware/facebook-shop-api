<?php

class Kivi_FacebookShop_Model_System_Config_Color
{

	public function toOptionArray()
	{

		$options = array(
			array(
				'label' => 'beige',
				'value' => 'beige',
			),
			array(
				'label' => 'blue',
				'value' => 'blue',
			),
			array(
				'label' => 'brown',
				'value' => 'brown',
			),
			array(
				'label' => 'celadon',
				'value' => 'celadon',
			),
			array(
				'label' => 'cherry',
				'value' => 'cherry',
			),
			array(
				'label' => 'cyan',
				'value' => 'cyan',
			),
			array(
				'label' => 'dark',
				'value' => 'dark',
			),
			array(
				'label' => 'gray',
				'value' => 'gray',
			),
			array(
				'label' => 'green',
				'value' => 'green',
			),
			array(
				'label' => 'navy',
				'value' => 'navy',
			),
			array(
				'label' => 'olive',
				'value' => 'olive',
			),
			array(
				'label' => 'orange',
				'value' => 'orange',
			),
			array(
				'label' => 'peach',
				'value' => 'peach',
			),
			array(
				'label' => 'pink',
				'value' => 'pink',
			),


		);

		return $options;
	}

}