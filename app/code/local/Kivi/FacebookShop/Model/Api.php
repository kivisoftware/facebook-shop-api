<?php


class Kivi_FacebookShop_Model_Api extends Mage_Core_Model_Abstract
{

	const XML_FACEBOOKSHOP_PREFIX = 'facebookshop/';

	public function getConfigs()
	{
		$lists = array(
			'facebookshop/general/enable',
			'facebookshop/general/root',
			'facebookshop/style/color',
			'facebookshop/header/logo',
			'facebookshop/header/phone',
			'facebookshop/header/email',
			'facebookshop/header/facebook',
			'facebookshop/header/twitter',
			'facebookshop/header/googleplus',
			'facebookshop/body/category',
			'facebookshop/footer/col1_enable',
			'facebookshop/footer/col1_heading',
			'facebookshop/footer/col1_content',
			'facebookshop/footer/col2_enable',
			'facebookshop/footer/col2_heading',
			'facebookshop/footer/col2_content',
			'facebookshop/footer/col3_enable',
			'facebookshop/footer/col3_heading',
			'facebookshop/footer/col3_content',
			'facebookshop/footer/col4_enable',
			'facebookshop/footer/col4_heading',
			'facebookshop/footer/col4_content',
			'facebookshop/footer/copyright',
		);

		$configs = array();
		foreach ($lists as $_row) {
			$configs[$_row] = Mage::getStoreConfig($_row);
		}

		return $configs;
	}

	public function getConfig($name)
	{
		return Mage::getStoreConfig($name);
	}

	public function encode($data)
	{
		return Mage::helper('core')->jsonEncode($data);
	}

	public function getProducts()
	{
		$category_id = Mage::app()->getRequest()->getParam('category_id');
		$collection  = Mage::getModel('catalog/product')->getCollection();
		if ($category_id) {
			$collection
				->joinField('category_id', 'catalog/category_product', 'category_id', 'product_id = entity_id', null, 'left')
				->addAttributeToSelect('*')
				->addAttributeToFilter('category_id', $category_id);
		}

		$products = array();
		foreach ($collection as $_product) {
			$_product                     = Mage::getModel('catalog/product')->load($_product->getId());
			$products[$_product->getId()] = $_product->getData();
		}

		return $products;
	}


	public function getCategories()
	{
		$collection = Mage::getModel('catalog/category')
			->getCollection()
			->addAttributeToSelect('*')
			->addIsActiveFilter();
		$categories = array();
		foreach ($collection as $category) {
			$categories[$category->getId()] = $category->getData();
		}

		return $categories;
	}

	public function getCategory($id)
	{
		$category = Mage::getModel('catalog/category')->load($id);

		return ($category->getId()) ? $category->getData() : null;
	}

	public function getMenuItems()
	{
		$collection = Mage::getModel('catalog/category')
			->getCollection()
			->addAttributeToSelect('*')
			->addFieldToFilter('include_in_menu', 1)
			->addFieldToFilter('parent_id', $this->getRootCategoryId())
			->addIsActiveFilter();

		$categories = array();
		foreach ($collection as $cat) {
			$item = $cat->getData();
			if ($cat['children_count']) {
				$item['subcats'] = array();
				$subs            = Mage::getModel('catalog/category')
					->getCollection()
					->addAttributeToSelect('*')
					->addFieldToFilter('include_in_menu', 1)
					->addFieldToFilter('parent_id', $cat->getId())
					->addIsActiveFilter();
				foreach ($subs as $sub) {
					$item['subcats'][] = $sub->getData();
				}
			}
			$categories[] = $item;

		}

		return $categories;
	}

	/**
	 * Default is 3
	 * @return int|mixed
	 */
	public function getRootCategoryId()
	{
		return $this->getConfig('facebookshop/general/root') ? $this->getConfig('facebookshop/general/root') : 3;
	}


}