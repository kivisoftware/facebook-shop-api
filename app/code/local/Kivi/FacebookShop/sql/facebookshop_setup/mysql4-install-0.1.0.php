<?php
/**
 * Kivi
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the Kivi.com license that is
 * available through the world-wide-web at this URL:
 * http://kivi.co.il/license-agreement/
 * 
 * DISCLAIMER
 * 
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 * 
 * @category    Kivi
 * @package     Kivi_FacebookShop
 * @copyright   Copyright (c) 2014 Kivi (http://kivi.co.il/)
 * @license     http://kivi.co.il/license-agreement/
 */

/** @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/**
 * create facebookshop table
 */
$installer->run("

");

$installer->endSetup();

